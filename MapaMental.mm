<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Engenharia de Software" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1070241052" CREATED="1521682914157" MODIFIED="1521930256893"><hook NAME="MapStyle" zoom="2.0">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Placeholders" LOCALIZED_STYLE_REF="defaultstyle.floating" FOLDED="true" POSITION="right" ID="ID_1177437592" CREATED="1521929960608" MODIFIED="1521930102569" HGAP_QUANTITY="41.999998748302495 pt" VSHIFT_QUANTITY="-95.24999716132888 pt">
<hook NAME="FreeNode"/>
<node TEXT="placeholders de processo" FOLDED="true" ID="ID_1906655843" CREATED="1521684112700" MODIFIED="1521684118096">
<node TEXT="Desenvolvedores" ID="ID_899136196" CREATED="1521930059845" MODIFIED="1521930066579"/>
<node TEXT="Gerentes" ID="ID_25723350" CREATED="1521930067046" MODIFIED="1521930071951"/>
<node TEXT="Testadores" ID="ID_1801403404" CREATED="1521930073907" MODIFIED="1521930077073"/>
<node TEXT="Arquiteto" ID="ID_524918328" CREATED="1521930077416" MODIFIED="1521930086285"/>
<node TEXT="Operadores" ID="ID_787133887" CREATED="1521930086717" MODIFIED="1521930095000"/>
</node>
<node TEXT="placeholders de produto" FOLDED="true" ID="ID_1345017192" CREATED="1521684097295" MODIFIED="1521684112193">
<node TEXT="Cliente" ID="ID_32992486" CREATED="1521930045990" MODIFIED="1521930048559"/>
<node TEXT="Usu&#xe1;rios" ID="ID_12245253" CREATED="1521930048884" MODIFIED="1521930053544"/>
</node>
</node>
<node TEXT="Requisitos" FOLDED="true" POSITION="right" ID="ID_718273801" CREATED="1521684068632" MODIFIED="1521928744209">
<node TEXT="Funcionais" ID="ID_1423347411" CREATED="1521684162495" MODIFIED="1521928758594"/>
<node TEXT="N&#xe3;o-Funcionais" ID="ID_877591752" CREATED="1521684166886" MODIFIED="1521684170945"/>
</node>
<node TEXT="Processos" FOLDED="true" POSITION="left" ID="ID_323876514" CREATED="1521684082530" MODIFIED="1521684087731">
<node TEXT="Processo Unificado" FOLDED="true" ID="ID_1493601471" CREATED="1521684121565" MODIFIED="1521929588311">
<node TEXT="RUP" ID="ID_694631480" CREATED="1521684130956" MODIFIED="1521929634349">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1242245134" STARTINCLINATION="96;0;" ENDINCLINATION="96;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="OpenUP" ID="ID_1694631546" CREATED="1521684132877" MODIFIED="1521929638617">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1242245134" STARTINCLINATION="96;0;" ENDINCLINATION="96;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
</node>
<node TEXT="FDD" ID="ID_1098478100" CREATED="1521684200350" MODIFIED="1521929620780">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1242245134" STARTINCLINATION="243;0;" ENDINCLINATION="243;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="XP" FOLDED="true" ID="ID_1046144570" CREATED="1521684204216" MODIFIED="1521929563624">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1242245134" STARTINCLINATION="263;0;" ENDINCLINATION="263;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<node TEXT="XP+Scrum" ID="ID_5721546" CREATED="1521684206935" MODIFIED="1521684210407"/>
</node>
</node>
<node TEXT="Modelos de Ciclo de Vida" FOLDED="true" POSITION="left" ID="ID_141669442" CREATED="1521684072666" MODIFIED="1521929971609" MAX_WIDTH="129.74999631196272 pt" MIN_WIDTH="129.74999631196272 pt">
<node TEXT="Iterativo e Incremental" ID="ID_1242245134" CREATED="1521684143666" MODIFIED="1521684149135"/>
<node TEXT="Espiral" ID="ID_1968762487" CREATED="1521684149980" MODIFIED="1521684152478"/>
<node TEXT="Cascata" ID="ID_1029330015" CREATED="1521684153122" MODIFIED="1521684154739"/>
</node>
</node>
</map>
